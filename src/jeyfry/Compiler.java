package jeyfry;
import java.io.Console;
import java.util.ArrayList;
abstract class Compiler {
	static boolean HAVE_CONSOLE=true;
	static int STREAM_SIZE;
	static int VARIABLE_SIZE;
	static boolean DEBUG_MODE;
	static Console console = System.console();
	static ArrayList<Token> tokenStream = new ArrayList<Token>();
	static ArrayList<Variable> vars = new ArrayList<Variable>();
	static String readText(String s){
		String text="";
		if (Compiler.console==null) {
			HAVE_CONSOLE = false;
			Interpreter.exPrint(s);
        	try{
        		int inChar = System.in.read();	// Type text       	
	            while (inChar!=13){				// Yeah, right this way
	            	text+=(char)inChar;			// Hardcore, isn't it?
	            	inChar = System.in.read();	// For eclipse users
	            }	
        	}catch(Exception e){Interpreter.exPrint(A.IO_ERROR+" reason: "+e.getMessage());System.exit(0);}
		}
		else text =Compiler.console.readLine(s); // For console users
		return text;
	}
	static Variable getVar(String s){
		boolean isArray=false;
		if (s.contains(",")) {
			isArray=true;
			int dot = s.indexOf(",");
			int index = Integer.parseInt(s.substring(dot+1));
			String name = s.substring(0,dot);			
			for (Variable x:vars)
				if (x.name.equalsIgnoreCase(name))
					if (x.i==index) return x;
		}
		for (Variable x:vars) if (x.name.equalsIgnoreCase(s)&!isArray) return x;		
		return null;
	}
	static int getValue(Token t){
		if (t.type==A.CONSTANT) return Integer.parseInt(t.expr);
		if (t.type==A.VARIABLE|t.type==A.ARRAY) return getVar(t.expr).value;		
		return 0;
	}
	static float getValueF(Token t){
		if (t.type==A.CONSTANT) return Float.parseFloat(t.expr);
		if (t.type==A.VARIABLE) 
			if (getVar(t.expr).type==A.TYPE_FLOAT) return getVar(t.expr).valuef;
			else return getVar(t.expr).value;
		return 0;
	}
	static Token getToken(int i){ return tokenStream.get(i); }		
	static Variable getVarByToken(int i){
		if (getToken(i).type==A.ARRAY) {
			int dot = getToken(i).expr.indexOf(",");
			String name = getToken(i).expr.substring(0,dot);
			int index = Integer.parseInt(getToken(i).expr.substring(dot+1));
			for (int j=0;j<VARIABLE_SIZE;j++)
				if (vars.get(j).name.equals(name))
					if (vars.get(j).i==index) return vars.get(j);

		}
		if (getToken(i).type==A.CONSTANT) 
			if (getToken(i).expr.contains(".")) return new Variable(A.TYPE_FLOAT,getValueF(getToken(i)),-1);
			else return new Variable(A.TYPE_INT,getValue(getToken(i)),-1);
		if (getToken(i).type==A.VARIABLE) return getVar(getToken(i).expr);
		return null;
	}
	static class Regexpr{
		String expr;
		int type;
		Regexpr(int type_,String expr_){
			expr=expr_;
			type=type_;
		}
	}
	static class Token{
		int type;
		String expr;
		Token(String expr_,int type_){
			this.expr =expr_;
			this.type =type_;
		}		
		static void setType(Token t, int i){ t.type=i; }
		static void setExpr(Token t, String s){ t.expr=s; }				
	}
	static class Variable{
		int type;
		String name;
		int value;
		float valuef;
		int i;
		Variable(int type_,String name_,int i_){
			type = type_;
			name = name_;
			i = i_;
		}
		Variable(int type_,float f_,int i_){
			type = type_;
			valuef = f_;	
			i = i_;
		}
		Variable(int type_,int v_,int i_){
			type = type_;
			value = v_;
			valuef = v_;
			i=i_;
		}
		static String get(Variable v){
			String s=" ";
			if (v.i!=-1) s= "["+v.i+"]";
			if (v.name==null) if (v.type==A.TYPE_FLOAT) return v.valuef+"";
			else return v.value+"";
			if (v.type==A.TYPE_FLOAT) return "FLOAT "+v.name+s+" = "+v.valuef;
			return "INT "+v.name+s+" = "+v.value;
		}
	}	
}
