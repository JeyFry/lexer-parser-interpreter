package jeyfry;

public class Parser {
	String syntaxErr;
	Parser(){
		System.gc();
		Interpreter.exPrint("Stream Parsing:");
		int counter=0;
		int lastc=0;
		int line=1;
		int errCount=0;
		while (counter<Compiler.STREAM_SIZE){		
			counter = getEnd(counter); // To next line			
			if (counter == -1) 
				if (Compiler.getToken(Compiler.STREAM_SIZE-1).type!=A.SEMICON){						
					Interpreter.exPrint(A.SEMICOLON_ERROR+" at line "+getLine(lastc));
					errCount++;
					break;	// Lost last semicon
				}else break;//The end
			int type = getType(lastc,counter);
			if (type==-1){ 
				Interpreter.exPrint(A.SEMICOLON_ERROR+" at line "+getLine(lastc));
				errCount++;
				break;
			}
			if (!syntax(type,lastc,counter-1)){
				errCount++;
				Interpreter.exPrint("Syntax error at line "+ line+" : "+syntaxErr+".");
				syntaxErr=null;
			}
			lastc = counter+1;
			line++;
		}
		Interpreter.exPrint("Syntax check complete. Errors: " + errCount+".");
		if (errCount!=0) System.exit(0);				
	}
	boolean syntax(int type,int b,int e){ //return true if in range of [B..E] tokens have no syntax error
		switch(type){
		case A.PROC:
			if ((b==e)&(Compiler.getToken(b).expr.contains("read"))) return true;
			if (compareType(e,A.VARIABLE) & (e==b+1)) return true;// PROC VAR
			if (compareType(e,A.CONSTANT) & !(Compiler.getToken(b).expr.contains("read"))& (e==b+1)) return true;// PROC VAR	
			if ((getType(b+1,e)!=-1)&(syntax(getType(b+1,e),b+1,e))) return true; //PROC MATH
			else {
				if (syntaxErr==null) syntaxErr=A.MATH_ERROR;
				return false;
			}
		case A.ASSIGN:
			if (compareType(b,A.VARIABLE)|compareType(b,A.ARRAY))
				if (compareType(b+1,A.ASSIGN)){
					if ((b==e-2) & ((compareType(b,A.CONSTANT))|(compareType(b,A.VARIABLE)) ) ) return true;// VAR = VAR(CONST)
					return syntax(getType(b+2,e),b+2,e);													// VAR = MATH
				}			
			if (syntaxErr==null) syntaxErr=A.ASSIGN_ERROR;
			break;
		case A.MATH:
			boolean t=false;
			int leftS=0;
			int rightS=0;
			if (b==e){
				if (compareType(b,A.VARIABLE)) return true;
				if (compareType(b,A.CONSTANT)) return true;
				if (compareType(b,A.ARRAY)) return true;
			}
			for (int i=b;i<=e;i++){
				if (Compiler.getToken(i).expr.contains("(")) leftS++;
				if (Compiler.getToken(i).expr.contains(")")) rightS++;
			}
			if (leftS==rightS){
				for (int i=b;i<=e;i++)
					if (Compiler.getToken(i).expr.contains("("))
						for (int j=e;j>=b;j--)
							if (Compiler.getToken(j).expr.contains(")"))
								t = syntax(A.MATH,i+1,j-1);	// (MATH)	
			}else {syntaxErr=A.BRACKET_ERROR;return false;}
			if (e-b<=2)
				if (compareType(b+1,A.MATH))												
					if (compareType(b,A.VARIABLE)|compareType(b,A.CONSTANT)|compareType(b,A.ARRAY)) 		// VAR(CONST) MATH
						if (compareType(b+2,A.VARIABLE)|compareType(b+2,A.CONSTANT)|compareType(b+2,A.ARRAY)) t=true; // VAR(CONST) MATH VAR(CONST)
			if (e-b>2)
				if (t)return t;
				else {
					t=true; 
					for (int i=b;i<e;i++) if (Compiler.getToken(i).type==Compiler.getToken(i+1).type) t = false;
				}
			if (division(b)){																	//Division
				syntaxErr = A.DIVISION_BY_ZERO;
				return false;				
			}else{
				if (syntaxErr==null) syntaxErr = A.MATH_ERROR;
				return t;
			}
		default: syntaxErr=A.UNKNOWN_TYPE_ERROR;break;
		}
		return false;
	}
	String getLine(int b){	//return number of the line and number of the Token in da line
		int c=1;
		for (int i=0;i<=b;i++) if (Compiler.getToken(i).type==A.SEMICON) c++;
		return c+" ("+b+")";
	}
	boolean division (int b){ //return true if expression have division by zero
		boolean isTruePosition = compareType(b+2,A.CONSTANT); 
		boolean isZero = Compiler.getToken(b+2).expr.contains("0");
		boolean isDivision = Compiler.getToken(b+1).expr.contains("/");		
		return isTruePosition & isDivision & isZero;
	}
	boolean compareType (int i,int type){ // compare type of a token
		return Compiler.getToken(i).type==type;
	}
	static boolean isFloat (int b,int e){	// return false if expression return float type		
		for (int i=b;i<=e;i++){			
			if (Compiler.getToken(i).expr.contains(".")) return true;
			if (Compiler.getToken(i).type==A.VARIABLE)
				if (Compiler.getVarByToken(i).type==A.TYPE_FLOAT) return true;
			if (Compiler.getToken(i).expr.contains("/")) return true;			
			}
		return false;
	}
	static int getType(int b,int e){ //return type of expression
		boolean t=false;
		boolean t_=false;
		if (b==e) return A.MATH;
		for (int i=b;i<=e;i++){
			if (Compiler.getToken(i).type==A.PROC) if (t) return -1;
				else t=true;
			if (Compiler.getToken(i).type==A.ASSIGN) if (t_) return -1;
				else t_=true;
		}
		if (t&t_) return -1;
		if (Compiler.getToken(b).type==A.PROC) return A.PROC;
		if (Compiler.getToken(b+1).type==A.ASSIGN) return A.ASSIGN;
		if (e>b+4)if (Compiler.getToken(b+4).type==A.ASSIGN) return A.ASSIGN;
		for (int i=b;i<=e;i++){			
			if ((Compiler.getToken(i).type==A.MATH)&(!t)&(!t_)) return A.MATH;									
		}
		return -1;		
	}
	static int getEnd(int c){ //return int next semicon
		while (c<Compiler.STREAM_SIZE-1) {
			c++;
			if (Compiler.getToken(c).type==A.SEMICON) return c;
		}
		return -1;
	}
}
