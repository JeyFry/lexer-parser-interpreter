package jeyfry;
import java.util.ArrayList;
import jeyfry.Compiler.*;
public class Interpreter {
	static int exCalc(ArrayList<Token> whatToCalc){ // return int result of expression with brackets
		int b_i=-1;
		int b_open=0;
		int e_i=-1;
		int e_open=0;
		int i=0;	
		int a_;
		int b_;
		ArrayList<Token> exprToCalc = new ArrayList<Token>();
		if (whatToCalc.size()==3){  // A = C + B
			String math;
			math = whatToCalc.get(1).expr;			
			a_ = Compiler.getValue(whatToCalc.get(0));
			b_ = Compiler.getValue(whatToCalc.get(2));
			return calc(math,a_,b_);
		}
		for (Token x:whatToCalc){	//Brace
			if (x.expr.contains("(")){
				if (b_open ==e_open) b_i=i;
				b_open++;
			}
			if (x.expr.contains(")")) {
				if (b_open==e_open+1) {
					e_i = i;						
					for(int j=b_i+1;j<e_i;j++) exprToCalc.add(whatToCalc.get(j));
					int clc = exCalc(exprToCalc);
					ArrayList<Token> result = new ArrayList<Token>();
					for(int j=0;j<b_i;j++) result.add(whatToCalc.get(j));
					result.add(new Token(clc+"",A.CONSTANT));
					for(int j= e_i+1;j<whatToCalc.size();j++) result.add(whatToCalc.get(j));					
					return exCalc(result);
				}
				e_open++;
			}
			i++;
		}
		i=0;
		for (Token x:whatToCalc){	//Check for *
			if (x.expr.contains("*")){
				int clc = calc("*",Compiler.getValue(whatToCalc.get(i-1)),Compiler.getValue(whatToCalc.get(i+1)));								
				for (int j=0;j<i-1;j++) exprToCalc.add(whatToCalc.get(j));				
				exprToCalc.add(new Token(clc+"",A.CONSTANT));				
				for (int j=i+2;j<whatToCalc.size();j++) exprToCalc.add(whatToCalc.get(j));
				return exCalc(exprToCalc);
			}
			i++;
		}		
		b_ = Compiler.getValue(whatToCalc.get(whatToCalc.size()-1));
		String math = whatToCalc.get(whatToCalc.size()-2).expr;
		for (i=0;i<whatToCalc.size()-1;i++)exprToCalc.add(whatToCalc.get(i));
		return calc(math,exCalc(exprToCalc),b_);
	}
	static float exCalcF(ArrayList<Token> whatToCalc){ // return float result of expression with brackets
		int b_i=-1;
		int b_open=0;
		int e_i=-1;
		int e_open=0;
		int i=0;			
		ArrayList<Token> exprToCalc = new ArrayList<Token>();
		if (whatToCalc.size()==3){  // A = C + B
			String math = whatToCalc.get(1).expr;
			Variable a_;
			Variable b_;
			if (whatToCalc.get(0).type==A.VARIABLE)
				a_ = Compiler.getVar(whatToCalc.get(0).expr);
			else
				a_ = new Variable(A.TYPE_FLOAT,Float.parseFloat(whatToCalc.get(0).expr),-1);
			if (whatToCalc.get(2).type==A.VARIABLE)
				b_ = Compiler.getVar(whatToCalc.get(2).expr);
			else
				b_ = new Variable(A.TYPE_FLOAT,Float.parseFloat(whatToCalc.get(2).expr),-1);						
			return calcF(math,a_,b_);
		}			
		for (Token x:whatToCalc)	{
			if (x.expr.contains("(")){
				if (b_open ==e_open) b_i=i;
				b_open++;
			}
			if (x.expr.contains(")")) {
				if (b_open==e_open+1) {
					e_i = i;											
					for(int j=b_i+1;j<e_i;j++) exprToCalc.add(whatToCalc.get(j));
					float calc = exCalcF(exprToCalc);
					ArrayList<Token> result = new ArrayList<Token>();
					for(int j=0;j<b_i;j++) result.add(whatToCalc.get(j));
					result.add(new Token(calc+"",A.CONSTANT));
					for(int j= e_i+1;j<whatToCalc.size();j++) result.add(whatToCalc.get(j));					
					return exCalcF(result);
				}
				e_open++;
			}
			i++;
		}		
		i=0;
		for (Token x:whatToCalc){	//Check for *
			if (x.expr.contains("*")|x.expr.contains("/")){
				Variable a = new Variable(A.TYPE_FLOAT,Compiler.getValueF(whatToCalc.get(i-1)),-1);
				Variable b = new Variable(A.TYPE_FLOAT,Compiler.getValueF(whatToCalc.get(i+1)),-1);
				float clc = calcF(x.expr,a,b);								
				for (int j=0;j<i-1;j++) exprToCalc.add(whatToCalc.get(j));				
				exprToCalc.add(new Token(clc+"",A.CONSTANT));				
				for (int j=i+2;j<whatToCalc.size();j++) exprToCalc.add(whatToCalc.get(j));
				return exCalcF(exprToCalc);
			}
			i++;
		}		
		Variable b_ = new Variable(A.TYPE_FLOAT,Compiler.getValueF(whatToCalc.get(whatToCalc.size()-1)),-1);
		String math = whatToCalc.get(whatToCalc.size()-2).expr;
		for (i=0;i<whatToCalc.size()-1;i++)exprToCalc.add(whatToCalc.get(i));
		Variable a_ = new Variable(A.TYPE_FLOAT,exCalcF(exprToCalc),-1);
		return calcF(math,a_,b_);
	}	
	@Deprecated
	static int calc(String math, int a,int b){// result = a "math" b		
		int c=0;
		switch (math){
		case "+":c = a+b;break;
		case "-":c = a-b;break;
		case "*":c = a*b;break;
		case "/":c = a/b;break;
		default:break;
		}
		return c;
	}
	static float calcF(String math,Variable a_,Variable b_){// result = a_ "math" b_
		float a,b;
		if (a_.type==A.TYPE_FLOAT) a = a_.valuef; else a = a_.value;
		if (b_.type==A.TYPE_FLOAT) b = b_.valuef; else b = b_.value;
		float c=0;		
		switch (math){		
		case "+":c = a+b;break;
		case "-":c = a-b;break;
		case "*":c = a*b;break;
		case "/":c = a/b;break;
		default:break;
		}	
		return c;
	}	
	Interpreter(){		
		System.gc();
        if (Compiler.DEBUG_MODE) {
        	String s;
        	Interpreter.exPrint("");
        	for (int i=0;i<Compiler.STREAM_SIZE;i++){ 
        		switch(Compiler.getToken(i).type){
        		case 1:s="VARIABLE";break;
        		case 2:s="CONSTANT";break;
        		case 3:s="MATH";break;
        		case 4:s="ASSIGN";break;
        		case 5:s="PROC";break;
        		case 6:s="BRACES";break;
        		case 7:s="BRACKET";break;
        		case 8:s="SEMICON";break;
        		case 9:s="SPACE";break;
        		case 0xD:s="ARRAY";break;
        		default:s="UNKNOWN";break;
        		}
        		Interpreter.exPrint(Compiler.getToken(i).expr+" of "+s);
        	}        		     
        }
		Interpreter.exPrint("Executing:");		
		for (int i=0;i<Compiler.STREAM_SIZE;i++){ // Find Variables			
			if (Compiler.getToken(i).type==A.ARRAY){	//If array found
				int dot = Compiler.getToken(i).expr.indexOf(",");
				String newVarName = Compiler.getToken(i).expr.substring(0,dot);
				int index = Integer.parseInt(Compiler.getToken(i).expr.substring(dot+1));				
				boolean t=true;				
				for (int j=0;j<Compiler.VARIABLE_SIZE;j++)						//check other for arrays
					if (newVarName.equalsIgnoreCase(Compiler.vars.get(j).name))						
						if (Compiler.vars.get(j).i==index) t = false;								
				if (t) Compiler.vars.add(new Compiler.Variable(A.TYPE_INT,newVarName,index));
			}
			if (Compiler.getToken(i).type==A.VARIABLE){// if variable found	
				String newVarName = Compiler.getToken(i).expr;
				boolean t=true;	
				for (Variable x:Compiler.vars) if (newVarName.equalsIgnoreCase(x.name)) t=false;// check all vars and if same name then don't add
				if (t) Compiler.vars.add(new Compiler.Variable(A.TYPE_INT,newVarName,-1)); // if no then add
			}
			Compiler.VARIABLE_SIZE = Compiler.vars.size();
		}
		int counter=0;
		int lastc=0;
		while (counter<Compiler.STREAM_SIZE){
			counter = Parser.getEnd(counter);
			if (counter ==-1) break;
			if (counter>=Compiler.STREAM_SIZE) break;
			if (lastc>=Compiler.STREAM_SIZE) break;
			if (Parser.getType(lastc,counter)==A.ASSIGN) // Set type = float if have division
				for (int i=lastc;i<counter;i++){					
					if (Compiler.getToken(i).expr.contains("/")) Compiler.getVarByToken(lastc).type=A.TYPE_FLOAT;
					if (Compiler.getToken(i).type==A.VARIABLE)
						if (Compiler.getVarByToken(i).type==A.TYPE_FLOAT) Compiler.getVarByToken(lastc).type=A.TYPE_FLOAT;
				}
			if (Compiler.DEBUG_MODE) {
				for (int i=lastc;i<=counter;i++)System.out.print(Compiler.getToken(i).expr+" ");
				Interpreter.exPrint("");
			}			
			try{				
				execute(Parser.getType(lastc,counter-1),lastc,counter-1);
			}catch(Exception e){Interpreter.exPrint(A.UNKNOWN_ERROR+" "+e.toString());System.exit(0);}
			lastc = counter+1;
		}
	}
	void execute(int type,int b,int e){//begin end 
		switch(type){
		case A.PROC: 
			String procedure = Compiler.getToken(b).expr;
			switch(procedure){
			case "print":
				if (e==b+1){
					Variable var = Compiler.getVarByToken(b+1);   //PRINT F
					exPrint(var);
				}else{					
					if (Parser.isFloat(b+1,e)) exPrint(""+prepareAndCalcF(b+1,e));  
					else exPrint("" + prepareAndCalc(b+1,e));			//PRINT F+A+1
				}
				break;
			case "read":
				if (Compiler.getToken(b+1).type!=A.SEMICON) exRead(b+1);
				else {exRead("Type something. Please. ");}
				break;
			case "fft":
				//ADD furie
				if (e-b==1){
					String arr = Compiler.getVarByToken(e).name;
					int size=0;					
					for (Variable x:Compiler.vars) if (x.name.equals(arr)) size++;
					Float[] whatToFFT = new Float[size];
					int i=0;
					for (Variable x:Compiler.vars) 
						if (x.name.equals(arr)){
							if (x.type==A.TYPE_INT)whatToFFT[i]=x.value+0f;
							if (x.type==A.TYPE_FLOAT)whatToFFT[i]=x.valuef;
							i++;
						}
					Float[] result;
					try{
						result = fft(whatToFFT);
						String out = "FFT result:";
						for (i=0;i<size/2;i++) out+=" "+result[i];
						exPrint(out);
					}catch(Exception ex){exPrint(A.FFT_ERROR);System.exit(0);}
				}else{exPrint(A.PROC_ERROR+" ");System.exit(0);}
				
				break;
			}
			break;		
		case A.ASSIGN:
			if (e<=b+2){   // X = A	
				if (Compiler.getToken(b).type==A.VARIABLE|Compiler.getToken(b).type==A.ARRAY){
					if (Compiler.getToken(e).expr.contains(".")) Compiler.getVarByToken(b).valuef=Compiler.getValueF(Compiler.getToken(e));
					else {
						if (Compiler.getVarByToken(b).type==A.TYPE_INT) Compiler.getVarByToken(b).value=Compiler.getValue(Compiler.getToken(e));									
						else  Compiler.getVarByToken(b).valuef=Compiler.getValueF(Compiler.getToken(e));				
					}
				}
			}else{	// VAR = MATH				
				if (Parser.isFloat(b,e)) Compiler.getVarByToken(b).valuef = prepareAndCalcF(b+2,e);
				else Compiler.getVarByToken(b).value = prepareAndCalc(b+2,e);			
			}
			break;
		case A.MATH:break; // Nothing to do here :3
		default:Interpreter.exPrint(A.UNKNOWN_ERROR+" at "+b);break;
		}
		if (Compiler.DEBUG_MODE){
			for (int i=0;i<Compiler.VARIABLE_SIZE;i++) Interpreter.exPrint((i+1) +": "+Variable.get(Compiler.vars.get(i)));
			Interpreter.exPrint("---------------------");
		}
	}
	void exPrint(Variable a){
		exPrint(Variable.get(a));
	}
	static void exPrint(String s){
		if (Compiler.HAVE_CONSOLE) Compiler.console.printf(s+"\n");
		else System.out.println(s);
	}
	void exRead(int index){		
		assignVar(index,Compiler.readText("Type variable "+Compiler.getToken(index).expr+" = "));
		Interpreter.exPrint(Variable.get(Compiler.getVarByToken(index)));   
	}
	void assignVar(int index,String value_){ // VAR = (INT OR FLOAT)STRING
        boolean isFloat=true;	
    	if (!Compiler.HAVE_CONSOLE) value_ = value_.substring(1);
        float valuef = Float.parseFloat(value_);
    	int value=0;
    	try{
	        try{
	        	if (Compiler.getVarByToken(index).type!=A.TYPE_FLOAT) value = Integer.parseInt(value_);
	        	isFloat = false;
	        }catch(Exception e){}
	        if (isFloat){	        	
	        	Compiler.getVarByToken(index).type=A.TYPE_FLOAT;
	        	Compiler.getVarByToken(index).valuef=valuef;
	        }else{
	           	Compiler.getVarByToken(index).type=A.TYPE_INT;
	        	Compiler.getVarByToken(index).value=value;
	        }
    	}catch(Exception e){Interpreter.exPrint(A.IO_ERROR+" reason: "+e.getMessage());System.exit(0);}
	}
	static boolean exRead(String s){
		if (Compiler.readText(s).toLowerCase().contains("y")) return true;
		return false;
	}
	int prepareAndCalc(int b,int e){	// prepare for exCalc	
		ArrayList<Token> t = new ArrayList<Token>();
		for(int i=b;i<=e;i++) t.add(Compiler.getToken(i));		
		return exCalc(t);
	}
	float prepareAndCalcF(int b,int e){ // prepare for exCalcF	
		ArrayList<Token> t = new ArrayList<Token>();
		for(int i=b;i<=e;i++) t.add(Compiler.getToken(i));		
		return exCalcF(t);
	}
    public static Float[] fft(Float[] whatToFFT) {
    	int N = whatToFFT.length;
    	if (N % 2 !=0) return null;
        Complex[] x = new Complex[N];
        for (int i = 0; i < N; i++) {
            x[i] = new Complex(whatToFFT[i], 0);            
        }
        Complex[] y = fft(x);
        Float[] res = new Float[N/2];
        for (int i=0;i<N/2;i++) res[i] = (float)y[i].abs();
        return res;

    }
    public static Complex[] fft(Complex[] x) {
        int N = x.length; 
        if (N == 1) return new Complex[] { x[0] };
        if (N % 2 != 0) throw new RuntimeException("N is not power of 2");
        Complex[] even = new Complex[N/2];
        for (int k = 0; k < N/2; k++) even[k] = x[2*k];
        
        Complex[] q = fft(even);       
        Complex[] odd  = even;  
        for (int k = 0; k < N/2; k++) odd[k] = x[2*k + 1];        
        Complex[] r = fft(odd);
        Complex[] y = new Complex[N];
        for (int k = 0; k < N/2; k++) {
            double kth = -2 * k * Math.PI / N;
            Complex wk = new Complex(Math.cos(kth), Math.sin(kth));
            y[k]       = q[k].plus(wk.times(r[k]));
            y[k + N/2] = q[k].minus(wk.times(r[k]));
        }
        return y;
    }
}

