package jeyfry;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jeyfry.Compiler.*;
public class Lexer {	
	static int getType(String s){
		Matcher matcher;
		Pattern pattern;
		for (Regexpr a : A.check){
			pattern = Pattern.compile(a.expr);
			matcher = pattern.matcher(s);
			if ( matcher.matches() ) return a.type;
		}
		return -1;
	}
	Lexer(String fileName){
		BufferedReader br;
    	System.out.print("File reading: ");
    	try {
    		br = new BufferedReader(new FileReader(fileName));
	        String line="",code="";
	        int iLine=-1;
	        while (line!=null) {	//Reading file
	        	if ((line!=null)&!(line.contains("#"))){
	        		code+=line+" ";
	        		iLine++;
	        	}	        	
	        	line = br.readLine();
	        }code = code.substring(1);
	        String[] t = code.split(" ");
	        for (int i=0;i<t.length;i++) {
	        	int x = getType(t[i]);
	        	if (x==-1) for (int j=0;j<t[i].length();j++) 
	        		if (getType(""+t[i].charAt(j))!=-1)Compiler.tokenStream.add(new Token(t[i].charAt(j)+"",getType(""+t[i].charAt(j))));
	        		else {
	        			Compiler.tokenStream.add(new Token(t[i].charAt(j)+"",A.UNKNOWN));
	        		}
	        	else Compiler.tokenStream.add(new Token(t[i],getType(t[i])));
	        }
	        Compiler.STREAM_SIZE = Compiler.tokenStream.size();
	        int j=0;
	        while (j<Compiler.STREAM_SIZE-1){ //Looking for VARS & CONSTS more than 1 letter
	        	if (((Compiler.getToken(j).type==A.VARIABLE)&(Compiler.getToken(j+1).type==A.VARIABLE))|((Compiler.getToken(j).type==A.CONSTANT)&(Compiler.getToken(j+1).type==A.CONSTANT))){
	        		Compiler.getToken(j).expr+=Compiler.getToken(j+1).expr;
	        		Compiler.tokenStream.remove(j+1);
	        		Compiler.STREAM_SIZE = Compiler.tokenStream.size();
	        	}
	        	if(j<Compiler.STREAM_SIZE-2) //Looking for FLOAT CONSTS
	        			if (Compiler.getToken(j).type==A.CONSTANT)
	        				if (Compiler.getToken(j+1).type==A.UNKNOWN){
	        					int i=j+2;
	        					while (Compiler.STREAM_SIZE>i)
	        						if (Compiler.getToken(i).type!=A.CONSTANT) break;	        						
	        						else i++;	
	        					String s="";
	        					for (int k=j;k<i;k++) s+=Compiler.getToken(k).expr;
	        					Compiler.getToken(j).expr=s;
	        					Compiler.getToken(j).type=A.CONSTANT;
	        					for (int k=j+1;k<i;k++) Compiler.tokenStream.remove(j+1);
	        					Compiler.STREAM_SIZE = Compiler.tokenStream.size();	        					
	        				}
	        	j++;
	        }
	        j=0;
	        while (j<Compiler.STREAM_SIZE-3){		//ARRAY FIX
	        	if (j<Compiler.STREAM_SIZE-3)
	        		if (Compiler.getToken(j).type==A.VARIABLE)
	        			if (Compiler.getToken(j+1).type==A.BRACES)
	        				if (Compiler.getToken(j+2).type==A.CONSTANT)
	        					if (Compiler.getToken(j+3).type==A.BRACES){
	        						Compiler.getToken(j).type = A.ARRAY;
	        						Compiler.getToken(j).expr = Compiler.getToken(j).expr+","+Compiler.getToken(j+2).expr;
	        						Compiler.tokenStream.remove(j+1);
	        						Compiler.tokenStream.remove(j+1);
	        						Compiler.tokenStream.remove(j+1);
	        						Compiler.STREAM_SIZE = Compiler.tokenStream.size();
	        					}
	        	j++;
	        }
	        Interpreter.exPrint("Successfully read "+iLine+" lines, found "+ Compiler.STREAM_SIZE+" tokens.");
    	}catch(Exception e){Interpreter.exPrint("Error: "+ A.FILE_ERROR +", reason: "+e.getLocalizedMessage());e.printStackTrace();System.exit(0);}
	}
}

