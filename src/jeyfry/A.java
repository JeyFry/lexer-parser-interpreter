package jeyfry;

import jeyfry.Compiler.Regexpr;
public interface A {	// Consts Interface		
	static String FILE_ERROR = "File open err 0x0";	//Errors
	static String PROC_ERROR = "Proc type err 0x1";
	static String ASSIGN_ERROR = "Assign type err 0x2";
	static String MATH_ERROR = "Math type err 0x3";
	static String UNKNOWN_TYPE_ERROR = "Unknown type err 0x4";
	static String DIVISION_BY_ZERO = "Division by zero err 0x5";
	static String SEMICOLON_ERROR = "Semicolon \";\" is lost err 0x6";
	static String UNKNOWN_ERROR = "Unknown err 0x7";
	static String BRACKET_ERROR = "Bracket err 0x8";
	static String IO_ERROR = "IO err 0x9";
	static String FFT_ERROR = "FFT err 0xA. Array must be 2^n size";
	static byte VARIABLE = 0x1;	//Types
	static byte CONSTANT= 0x2;
	static byte MATH= 0x3;
	static byte ASSIGN= 0x4;
	static byte PROC= 0x5;
	static byte BRACES = 0x6;	
	static byte BRACKET = 0x7;
	static byte SEMICON = 0x8;
	static byte SPACE = 0x9;
	static byte TYPE_FLOAT = 0xA;
	static byte TYPE_INT = 0xB;
	static byte UNKNOWN = 0xC;
	static byte ARRAY = 0xD;		// VAR_NAME , NUMBER
	final static Regexpr[] check = {	
		new Regexpr(SEMICON,";"),
		new Regexpr(SPACE," "),
		new Regexpr(ASSIGN,"="),
		new Regexpr(BRACES,"[\\[\\]]"),
		new Regexpr(BRACKET,"[()]"),
		new Regexpr(PROC,"print|read|fft"),		 
		new Regexpr(VARIABLE,"[a-zA-Z]+"),
		new Regexpr(CONSTANT,"([0-9]+/.[0-9])|([0-9]+)"),
		new Regexpr(MATH,"[/|+|-|/*]")
	};
}
